from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('<int:grocery_item_id>/', views.grocery_item, name="grocery_item"),

    # S03_Activity
    path('register', views.register, name="register"),
    path('change_password', views.change_password, name="change_password"),
    path('login', views.login_view, name="login_view"),
    path('logout', views.logout_view, name="logout")
]