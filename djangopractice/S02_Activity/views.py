from django.shortcuts import render, redirect
from django.http import HttpResponse
from .models import GroceryItem

from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout


# Create your views here.
def index(request):
    grocery_item_list = GroceryItem.objects.all()
    context = {
        'grocery_item_list': grocery_item_list,
        'user': request.user
    }
    return render(request, 'index.html', context)

def grocery_item(request, grocery_item_id):
    grocery_item = GroceryItem.objects.get(pk=grocery_item_id)
    context = { 'grocery_item': grocery_item }
    return render(request, 'grocery_item.html', context)


# S03_Activity
def register(request):
    users = User.objects.all()
    is_user_registered = False

    for user in users:
        if user.username == "chrlsptrck":
            is_user_registered = True
            break

    context = { "is_user_registered": is_user_registered }

    if is_user_registered == False:
        user = User()
        user.username = "chrlsptrck"
        user.first_name = "Charles Patrick"
        user.last_name = "Lilagan"
        user.email = "charles@mail.com"
        user.set_password("test")
        user.is_staff = False
        user.is_active = True
        user.save()

        context = {
            "first_name": user.first_name,
            "last_name": user.last_name
        }

    return render(request, "register.html", context)


def change_password(request):
    is_user_authenticated = False
    user = authenticate(username="johndoe", password="john1234")

    if user is not None:
        is_user_authenticated = True
        authenticated_user = User.objects.get(username=user.username)
        authenticated_user.set_password("john1234")
        authenticated_user.save()
    
    context = { "is_user_authenticated": is_user_authenticated }
    return render(request, "change_password.html", context)


def login_view(request):
    # Input username and password from the terminal
    username = input("Enter your username -> \t")
    password = input("Enter your password -> \t")
    user = authenticate(username = username, password = password)

    is_user_authenticated = True if user is not None else False
    context = {
        "is_user_authenticated": is_user_authenticated
    }

    if is_user_authenticated:
        login(request, user)
        return redirect("index")

    return render(request, "login.html", context)


def logout_view(request):
    logout(request)
    return redirect("index")



