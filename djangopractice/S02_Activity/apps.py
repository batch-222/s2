from django.apps import AppConfig


class S02ActivityConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'S02_Activity'
